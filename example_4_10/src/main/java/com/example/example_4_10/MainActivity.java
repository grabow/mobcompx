package com.example.example_4_10;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {

    TextView myTextView;
    int myGlobalCounter = 0;
    int startCounter = 0;
    AsyncThread myAsyncThread;
    Boolean doRun = true;
    String[] data = {"Hallo", "das", "ist", "ein", "schöner", "Tag"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myTextView = (TextView) findViewById(R.id.textView1);
        myTextView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        myAsyncThread = new AsyncThread();
        myTextView.setText("OK");
        myAsyncThread.execute(new Integer[] { (Integer) startCounter });
    }

    public class AsyncThread extends AsyncTask<Integer, String, Integer> {

        @Override
        protected Integer doInBackground(Integer... counter) {
            myGlobalCounter = (int) counter[0];

            while (doRun && (myGlobalCounter < 100 )) {

                Log.i("HAG", "Loop No. " + myGlobalCounter + ": " + data[myGlobalCounter % 6]);

                publishProgress("" + myGlobalCounter);



                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                myGlobalCounter++;
            }
            return (Integer) (myGlobalCounter);
        }

        protected void onProgressUpdate(String... values) {
            // TODO Auto-generated method stub
            TextView textView = (TextView) findViewById(R.id.textView1);
            Log.i("HAG", "onPU: " + myGlobalCounter);
            textView.setText( data[myGlobalCounter % 6]);
        }

        @Override
        protected void onPostExecute(Integer resultCounter) {
            myTextView.setText("Hallo Mr.: " + resultCounter);
            startCounter = resultCounter;
        }
    }

    public void onPause() {
        super.onPause();
        doRun = false;
    }
}