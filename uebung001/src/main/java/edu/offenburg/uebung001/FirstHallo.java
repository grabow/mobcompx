package edu.offenburg.uebung001;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

public class FirstHallo extends AppCompatActivity {

    String tag = "HAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_hallo);

        Date currentTime = Calendar.getInstance().getTime();

        TextView tv = findViewById(R.id.myTextView);
        tv.setText("Aktuelle Zeit: " + currentTime);

        Log.d(tag, "In the onCreate() event");
        Toast.makeText(this, "onCreate()",Toast.LENGTH_SHORT).show();

    }

    public void onStart()
    {
        super.onStart();
        Log.d(tag, "In the onStart() event");
        Toast.makeText(this, "onStart()",Toast.LENGTH_SHORT).show();

    }

    public void onRestart()
    {
        super.onRestart();
        Log.d(tag, "In the onRestart() event");
        Toast.makeText(this, "onRestart()",Toast.LENGTH_SHORT).show();

    }

    public void onResume()
    {
        super.onResume();
        Log.d(tag, "In the onResume() event");
        Toast.makeText(this, "onResume()",Toast.LENGTH_SHORT).show();

    }

    public void onPause()
    {
        super.onPause();
        Log.d(tag, "In the onPause() event");
        Toast.makeText(this, "onPause()",Toast.LENGTH_SHORT).show();

    }

    public void onStop()
    {
        super.onStop();
        Log.d(tag, "In the onStop() event");
        Toast.makeText(this, "onStop()",Toast.LENGTH_SHORT).show();

    }

    public void onDestroy()
    {
        super.onDestroy();
        Log.d(tag, "In the onDestroy() event");
        Toast.makeText(this, "onDestroy()",Toast.LENGTH_SHORT).show();

    }

}
