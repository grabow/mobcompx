package edu.offenburg.example_4_5;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {
    TextView myTextView;
    Handler handler;
    int counter = 0;

    String[] textF = {"Hallo", "dies", "ist", "gut!"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myTextView = (TextView) findViewById(R.id.textview);
        myTextView.setOnClickListener(this);


        handler = new Handler(message -> {
            int myCount = message.arg1;
            myTextView.setText( textF[myCount % 4]);
            return false;
        });
    }

    @Override
    public void onClick(View v) {
        new Thread(new Runnable() {
            public void run() {
                while (counter < 100000) {
                    Message myMsg = new Message();
                    myMsg.arg1 = counter++;

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    handler.sendMessage(myMsg);
                }
            }
        }).start();
    }
}
