package com.example.example_9_1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    BroadcastReceiver myBcr;

    public class MyBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isInitialStickyBroadcast()) {
                Log.i("HAG", "BCR - onReceive called!");
                Toast.makeText(context, "BCR called!!!", Toast.LENGTH_LONG).show();
            } else {
                Log.i("HAG", "BCR - old BC!");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        myBcr = new MyBroadcastReceiver();
        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        this.registerReceiver(myBcr, filter);
        Log.i("HAG", "BCR activated!");
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(myBcr);
        myBcr = null;
        Log.i("HAG", "BCR deactivated!");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}