package com.example.example_7_9;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("HAG", "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public int longProcess() {
        for (int i = 0; i < 100; i++) {
            Log.i("HAG", "Thread: " + Thread.currentThread().getName() + ", " + i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return 5;
    }

    Supplier<Integer> supplier = () -> {
        return longProcess();
    };

    Consumer<Integer> consumer = (i) -> {
        String res = "Fertig mit Nummer: " + i;
        Log.i("HAG", Thread.currentThread().getName() + ": " + res);
        Toast.makeText(this, res, Toast.LENGTH_SHORT).show();
    };

    public void onPressed(View v) throws ExecutionException, InterruptedException {
        Toast.makeText(this, "Pressed!", Toast.LENGTH_SHORT).show();
        Log.i("HAG", "Start...");

        // Run a task specified by a Runnable Object asynchronously.
        CompletableFuture<Void> cfuture =
                CompletableFuture.supplyAsync(()->longProcess())
                        .thenAcceptAsync(consumer);

        Log.i("HAG", " ... end");
    }
}