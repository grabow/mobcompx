package com.example.example_7_10;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("HAG", "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public int longProcess(int max) {
        for (int i = 0; i < max; i++) {
            Log.i("HAG", "Thread: " + Thread.currentThread().getName() + ", " + i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return max;
    }

    Supplier<Integer> mySupplier = () -> {
        return longProcess(100);
    };

    Runnable sayHallo = () -> {
        String res = "Fertig !!! ";
        Log.i("HAG", Thread.currentThread().getName() + ": " + res);
        Toast.makeText(this, res, Toast.LENGTH_SHORT).show();
    };

    public void onPressed(View v) throws ExecutionException, InterruptedException {
        Toast.makeText(this, "Pressed!", Toast.LENGTH_SHORT).show();
        Log.i("HAG", "Start...");

        Executor mainExecutor = this.getMainExecutor();

        // Run a task specified by a Runnable Object asynchronously.
        CompletableFuture<Integer> cf1 = CompletableFuture.supplyAsync(()->longProcess(50));
        CompletableFuture<Integer> cf2 = CompletableFuture.supplyAsync(()->longProcess(60));
        CompletableFuture<Integer> cf3 = CompletableFuture.supplyAsync(()->longProcess(30));

        // Wait until all are finishec
        CompletableFuture<Void> cfall = CompletableFuture.allOf(cf1, cf2, cf3)
                                        .thenRunAsync( sayHallo, mainExecutor )
                                        .thenRunAsync( () -> longProcess(50) )
                                        .thenRunAsync( sayHallo, mainExecutor );
        Log.i("HAG", " ... end");
    }
}