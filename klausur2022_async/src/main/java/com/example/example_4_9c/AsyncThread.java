package com.example.example_4_9c;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import androidx.core.util.Consumer;
import androidx.core.util.Supplier;

import java.util.function.Function;

public class AsyncThread extends AsyncTask<Integer, String, Integer> {

    boolean doRun = true;

    Function<Integer, Boolean> upFkt;
    AsyncThread(Function<Integer, Boolean> upFkt) {
        this.upFkt=upFkt;
    }

    int myGlobalCounter = 0;

    @Override
    protected Integer doInBackground(Integer... counter) {
        myGlobalCounter = (int) counter[0];
        while (doRun) {
            Log.i("HAG", "Loop No. " + myGlobalCounter + ": " + myGlobalCounter);
            publishProgress(String.valueOf(myGlobalCounter));
            myGlobalCounter++;
        }
        return (Integer) (myGlobalCounter);
    }

    @Override
    protected void onProgressUpdate(String... values) {
        // TODO Auto-generated method stub
        doRun = upFkt.apply( Integer.valueOf(values[0]) );
    }
}
