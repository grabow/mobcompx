package com.example.example_4_9c;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.Consumer;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.function.Function;
import java.util.function.Supplier;

public class MainActivity extends Activity implements View.OnClickListener {

    TextView myTextView;
    int startCounter = 1;
    AsyncThread myAsyncThread;
    long start = 0l;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myTextView = (TextView) findViewById(R.id.myTextView);
        myTextView.setOnClickListener(this);
        start = System.currentTimeMillis();
    }

    Function<Integer, Boolean> upFkt = (str) -> {
        myTextView = (TextView) findViewById(R.id.myTextView);
        myTextView.setText( str +"");
        if ((System.currentTimeMillis() - start) > 5000)
            return false;
        else
            return true;
    };

    @Override
    public void onClick(View v) {
        myAsyncThread = new AsyncThread(upFkt);
        myAsyncThread.execute(new Integer[] { (Integer) startCounter });
    }
}