package edu.offenburg.example_7_4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    ExecutorService executor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            Log.i("HAG", "onPause called!");
            executor.shutdownNow();
            // executor.awaitTermination(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onPressed(View v)  {
        Integer result = null;
        Toast.makeText(this, "Pressed!", Toast.LENGTH_SHORT).show();
        Log.i("HAG", "Start...");

        Callable<Integer> task = () -> {
            for (int i = 0; i < 100; i++) {
                Log.i("HAG", "Thread: " + Thread.currentThread().getName() + ", " + i);
                Thread.sleep(200);
            }
            return 123;
        };


        executor = Executors.newSingleThreadExecutor();

        Future<Integer> future = executor.submit(task);
        Log.i("HAG", "Future done? " + future.isDone());
        try {
            result = future.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i("HAG", "Result: " + result);
    }


}
