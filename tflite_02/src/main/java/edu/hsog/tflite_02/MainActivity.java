package edu.hsog.tflite_02;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.model.Model;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import edu.hsog.tflite_02.ml.ModelFloat16;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            Model.Options options = TFHelperHG.genOptions();
            ModelFloat16 model = ModelFloat16.newInstance(this, options);

            // Creates inputs for reference.
            TensorBuffer inputTensor = TensorBuffer.createFixedSize(new int[]{1, 224, 224, 3}, DataType.FLOAT32);

            Bitmap bt = TFHelperHG.getBitmapFromAsset(this, "elephant.jpg");
            TensorImage tensorImage = TFHelperHG.tensorFromBitmapWithAdjustment(bt);
            // For debugging
            Bitmap bt2 = tensorImage.getBitmap();
            inputTensor = tensorImage.getTensorBuffer();

            // Runs model inference and gets result.
            long start = System.currentTimeMillis();
            ModelFloat16.Outputs outputs = null;
            for (int i=0; i<100; i++) {
                outputs = model.process(inputTensor);
            }

            long duration = System.currentTimeMillis() - start;
            Log.i("HAG", "Time used: " + duration);

            TensorBuffer outputTensor = outputs.getOutputFeature0AsTensorBuffer();
            Log.i("HAG", "At 385: " + outputTensor.getFloatArray()[385] );
            Log.i("HAG" , "Size: " + outputTensor.getFloatArray().length);

            Map<String, Float> probMap = TFHelperHG.getProbMap(this, outputTensor);
            Stream<Map.Entry<String,Float>> sorted =
                    probMap.entrySet().stream()
                            .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                            .limit(5);
            sorted.forEach( (v)->Log.i("HAG", v.getKey() + ":" + v.getValue() ) );

            // Releases model resources if no longer used.
            model.close();
        } catch (IOException e) {
            // TODO Handle the exception
        }
    }
}