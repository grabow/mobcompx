package edu.offenburg.example_1_1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void buttonPressed(View v) {
        Log.i("HAG", "Button gedrückt!");
        Toast.makeText(this, "Button gedrückt!", Toast.LENGTH_LONG).show();
        Button myBtn = (Button) findViewById(R.id.myBtn);
        myBtn.setText("Pressed!!!!");
    }
}
