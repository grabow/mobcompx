package edu.offenburg.example_4_4;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {

    TextView myTextView;
    Handler handler;
    int counter = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myTextView = (TextView) findViewById(R.id.textView);
        myTextView.setOnClickListener(this);
        handler = new Handler();
    }

    @Override
    public void onClick(View v) {
        myTextView.setText("OK");

        Runnable myRun = new Runnable() {
            public void run() {
                counter++;
                myTextView.setText("Hello Mr. " + counter);
                handler.postDelayed(this, 3000);
            }
        };

        handler.postDelayed(myRun, 3000);
    }
}
