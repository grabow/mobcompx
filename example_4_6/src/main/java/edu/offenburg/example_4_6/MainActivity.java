package edu.offenburg.example_4_6;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView myTextView;
    Handler handler;
    int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myTextView = (TextView) findViewById(R.id.textView);

        handler = new Handler();

    }

    public void onPressed(View v) {

        Toast.makeText(this, "pressed", Toast.LENGTH_SHORT).show();
        myTextView.setText("OK");



        new Thread(new Runnable() {
            public void run() {

                while(true) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    counter++;

                    Runnable myRun = new Runnable() {
                        public void run() {
                            myTextView.setText("Das ist Counter: " + counter);
                        }
                    };

                    handler.post(myRun);
                }
            }
        }).start();
    }

}
