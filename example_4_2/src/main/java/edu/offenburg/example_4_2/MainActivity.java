package edu.offenburg.example_4_2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onPressed(View v) {

        Toast.makeText(this, "pressed", Toast.LENGTH_SHORT).show();

        Log.i("HAG", "GEDRÜCKT!!!!!!");

        for(int i=0; i<100000; i++) {

            Log.i("HAG", "i:" + i);

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
