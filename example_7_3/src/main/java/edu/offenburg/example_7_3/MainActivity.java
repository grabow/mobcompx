package edu.offenburg.example_7_3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    ExecutorService executor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            Log.i("HAG", "onPause called!");
            executor.shutdownNow();
            // executor.awaitTermination(5, TimeUnit.SECONDS);
            Log.i("HAG", "onPause finished!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onPressed(View v) {

        Toast.makeText(this, "Pressed!", Toast.LENGTH_SHORT).show();
        Log.i("HAG", "Start...");

        Runnable runnable = () -> {
            String name = Thread.currentThread().getName();
            for (int i = 0; i < 100; i++) {
                Log.i("HAG", "Thread: " + name + ", " + i);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        executor = Executors.newSingleThreadExecutor();

        executor.execute(runnable);

    }


}
