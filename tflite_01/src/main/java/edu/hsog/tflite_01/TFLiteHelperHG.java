package edu.hsog.tflite_01;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.gpu.CompatibilityList;
import org.tensorflow.lite.gpu.GpuDelegate;
import org.tensorflow.lite.support.common.FileUtil;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class TFLiteHelperHG {

    // Reads the tflite from the assets-folder and returns a MappedByteBuffer
    static public MappedByteBuffer loadTFModelFromAssets(Context context, String modelName) {
        FileChannel fileChannel = null;
        long startOffset;
        long declaredLength;
        try (AssetFileDescriptor fileDescriptor = context.getAssets().openFd(modelName)) {
            FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
            fileChannel = inputStream.getChannel();
            startOffset = fileDescriptor.getStartOffset();
            declaredLength = fileDescriptor.getDeclaredLength();
            return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    static public  Bitmap loadBitmapFromAsset(Context context, String filePath) {
        AssetManager assetManager = context.getAssets();
        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            // handle exception
        }
        return bitmap;
    }


    static public Bitmap resizeBitmap(Bitmap in, int xSize, int ySize, boolean bilinear) {
        return Bitmap.createScaledBitmap(in, xSize, ySize, bilinear);
    }

    static public ByteBuffer convertBitmapToByteBufferOfFloats(Bitmap bitmap, int sizeX, int sizeY, float mean, float diffMaxMin) {
        // 4 bytes for float32, pixelSize = 3 for RGB
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4 * 3 * sizeX * sizeY );
        byteBuffer.order(ByteOrder.nativeOrder());
        int[] intValues = new int[sizeX * sizeY];  //java int has 4 bytes <=> ARGB
        bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        int pixel = 0;
        for (int i = 0; i < sizeX; ++i) {
            for (int j = 0; j < sizeY; ++j) {
                final int val = intValues[pixel++];
                  float r = (((val >> 16) & 0xFF)-mean)/diffMaxMin;
                float g = (((val >> 8) & 0xFF)-mean)/diffMaxMin;
                float b = (((val) & 0xFF)-mean)/diffMaxMin;
                byteBuffer.putFloat(r);   //R
                byteBuffer.putFloat(g);   //G
                byteBuffer.putFloat(b);   //B
            }
        }
        return byteBuffer;
    }

    static int getIndexOfMax(float[] array) {
        int maxAt = 0;
        for (int i = 0; i < array.length; i++) {
            maxAt = array[i] > array[maxAt] ? i : maxAt;
        }
        return maxAt;
    }


    static public List<Recognition> getSortedResult(float[] labelProbArray, String[] labels, int maxResults, float threshold) {

        PriorityQueue<Recognition> pq =
                new PriorityQueue<>(
                        maxResults,
                        new Comparator<Recognition>() {
                            @Override
                            public int compare(Recognition lhs, Recognition rhs) {
                                return Float.compare(rhs.getConfidence(), lhs.getConfidence());
                            }
                        });

        for (int i = 0; i < labelProbArray.length; ++i) {
            float confidence = (labelProbArray[i]);
            if (confidence > threshold) {
                pq.add(new Recognition("" + i, labels[i] , confidence));
            }
        }

        final ArrayList<Recognition> recognitions = new ArrayList<>();
        int recognitionsSize = Math.min(pq.size(), maxResults);
        for (int i = 0; i < recognitionsSize; ++i) {
            recognitions.add(pq.poll());
        }

        return recognitions;
    }

    static public String[] loadLables(Context context, String fileName) {
        String[] labels = null;
        try {
            List<String> associatedAxisLabels = FileUtil.loadLabels(context , fileName);
            labels = associatedAxisLabels.toArray(new String[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return labels;
    }

    static public Interpreter.Options getInterpreterOptions(boolean gpu, int threads) {
        // Initialize interpreter with GPU delegate
        Interpreter.Options options = new Interpreter.Options();
        CompatibilityList compatList = new CompatibilityList();

        if(compatList.isDelegateSupportedOnThisDevice() && gpu) {
            // if the device has a supported GPU, add the GPU delegate
            GpuDelegate.Options delegateOptions = compatList.getBestOptionsForThisDevice();
            GpuDelegate gpuDelegate = new GpuDelegate(delegateOptions);
            options.addDelegate(gpuDelegate);
            Log.i("HAG", "GPU delegate");
        } else {
            Log.i("HAG", "No GPU delegate (default)");
        }
        if (threads!=0){
            // if the GPU is not supported, run on 4 threads
            options.setNumThreads(threads);
            Log.i("HAG", "Using threads: " + threads);
        } else {
            Log.i("HAG", "Using single thread (default)");
        }
        return options;
    }

}
