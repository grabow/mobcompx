package edu.hsog.tflite_01;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.InterpreterApi;
import org.tensorflow.lite.InterpreterApi.Options.TfLiteRuntime;
import org.tensorflow.lite.gpu.GpuDelegateFactory;
import org.tensorflow.lite.support.common.FileUtil;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private InterpreterApi interpreterApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MappedByteBuffer model = null;

        // InterpreterApi.Options options = new InterpreterApi.Options().addDelegateFactory(new GpuDelegateFactory());
        InterpreterApi.Options options = new InterpreterApi.Options();
        try {
            model = FileUtil.loadMappedFile(this, "model_float16.tflite");
            interpreterApi = InterpreterApi.create( model, options );
        } catch (IOException e) {
            e.printStackTrace();
        }

        Bitmap imgBitmap = TFLiteHelperHG.loadBitmapFromAsset(this, "elephant.jpg");
        imgBitmap = TFLiteHelperHG.resizeBitmap(imgBitmap, 224,224,false);
        ByteBuffer byteBuffer = TFLiteHelperHG.convertBitmapToByteBufferOfFloats(imgBitmap, 224, 224, 127.5f, 127.5f);
        float[][] result = new float[1][1000];

        long before = System.currentTimeMillis();
        for(int i=0; i<100; i++)
            interpreterApi.run(byteBuffer, result);
        long duration = System.currentTimeMillis() - before;
        Log.i("HAG", "Used time:" + duration);

        int maxAt = TFLiteHelperHG.getIndexOfMax(result[0]);
        Log.i("HAG", "Max at:" + maxAt);
        // LookUp for label at: https://storage.googleapis.com/download.tensorflow.org/data/imagenet_class_index.json

        String[] labels = TFLiteHelperHG.loadLables(this, "imagenet-labels.txt");
        List<Recognition> recognitions = TFLiteHelperHG.getSortedResult(result[0], labels, 5, 0);
        Log.i("HAG", recognitions.toString());
    }


}