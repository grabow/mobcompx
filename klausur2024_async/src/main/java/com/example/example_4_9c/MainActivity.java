package com.example.example_4_9c;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.core.util.Consumer;
import androidx.core.util.Supplier;

public class MainActivity extends Activity implements View.OnClickListener {

    TextView myTextView;
    AsyncThreadA myAsyncThread;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myTextView = (TextView) findViewById(R.id.myTextView1);
        myTextView.setOnClickListener(this);
    }

    public int doCountDown() {
        boolean doRun = true;
        int counter = 0;
        while (doRun) {
            Log.i("HAG", "Loop No. " + counter + ": " + counter);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            counter++;
            if (counter >= 100) doRun = false;
        }
        return counter;
    }

    Supplier<Integer> workFkt = () -> doCountDown();

    Consumer<Integer> upFkt = (i) -> {
        myTextView.setText( String.valueOf(i) );
    };
    @Override
    public void onClick(View v) {
        myAsyncThread = new AsyncThreadA(workFkt, upFkt);
        myAsyncThread.execute();
    }
}