package com.example.example_4_9c;

import android.os.AsyncTask;
import android.service.voice.VoiceInteractionService;
import android.util.Log;

import androidx.core.util.Consumer;
import androidx.core.util.Supplier;

public class AsyncThreadA extends AsyncTask<Void, Void, Integer> {
    Consumer<Integer> upFkt;
    Supplier<Integer> work;

    AsyncThreadA(Supplier<Integer> work, Consumer<Integer> upFkt) {
        this.work = work;
        this.upFkt = upFkt;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        int myCounter = work.get();
        return (Integer) (myCounter);
    }

    @Override
    protected void onPostExecute(Integer result) {
        upFkt.accept(result);
    }
}
