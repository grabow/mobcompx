package com.example.example_10_4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class ActivityShowHelp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_help);
    }

    public void onPressed2(View v) {
        Log.i("HAG", "onPressed2 called!");
        Intent myService = new Intent(this, MyService.class);
        stopService(myService);
    }
}