package com.example.example_10_4;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class MyService extends Service {

    boolean doRun = true;

    public MyService() {

    }



    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        int ONGOING_NOTIFICATION_ID = 1;
        startForeground(ONGOING_NOTIFICATION_ID, createNotification());

        Log.i("HAG", "onStartCommand called" );
        new Thread(() -> {
            int i = 0;
            while(doRun) {
                Log.i("HAG", "Foreground Service: " + i++);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        return Service.START_NOT_STICKY;
    }



    // Copy from example_10_2
    public Notification createNotification() {
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(this, ActivityShowHelp.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        createNotificationChannel();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "MyID1")
                .setSmallIcon(R.drawable.ic_alarm_add_24px)
                .setContentTitle("My notification")
                .setContentText("Hello HAG!")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        /*
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        / notificationId is a unique int for each notification that you must define
        notificationManager.notify(111, builder.build());
        */
        return builder.build();
    }


    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name =  "MyChannel1";
            String description =  "Less important notifications";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("MyID1", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public void onDestroy() {
        doRun=false;
        Log.i("HAG", "Service stopped...");
    }
}