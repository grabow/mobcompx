package edu.offenburg.example_6_6;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        new Thread( ()->{
            for(int i=1; i<10; i++) {

                int finalI = i;
                runOnUiThread(
                        ()-> Toast.makeText(this, "Hallo" + finalI, Toast.LENGTH_SHORT).show()
                );

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }   ).start();


    }
}
