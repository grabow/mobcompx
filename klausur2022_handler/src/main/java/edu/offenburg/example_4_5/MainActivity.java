package edu.offenburg.example_4_5;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {
    TextView myTextView;
    Handler handler;
    int counter = 9;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myTextView = (TextView) findViewById(R.id.textview);
        myTextView.setOnClickListener(this);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                int myCount = msg.arg1;
                myTextView.setText(myCount + "");

            }
        };
    }

    @Override
    public void onClick(View v) {
        new Thread(() -> {
            while (counter >= 0) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Message myMsg = new Message();
                myMsg.arg1 = counter--;
                handler.sendMessage(myMsg);
            }
        }).start();
    }
}
