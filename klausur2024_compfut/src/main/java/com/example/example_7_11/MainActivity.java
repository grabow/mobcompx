package com.example.example_7_11;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import java9.util.concurrent.CompletableFuture;
import java9.util.function.Supplier;

public class MainActivity extends AppCompatActivity {

    boolean doStop = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("HAG", "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public int longProcess(int id, int end) {

        TextView tv = findViewById(id);
        for (int i = 0; i < end; i++) {
            Log.i("HAG", "Thread: " + Thread.currentThread().getName() + ", " + i);
            final int finalI = i;
            runOnUiThread(() -> tv.setText(String.valueOf(finalI)));
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (doStop) break;
        }
        return end;
    }

    public void onPressed(View v) throws ExecutionException, InterruptedException {
        Toast.makeText(this, "Pressed!", Toast.LENGTH_SHORT).show();
        Log.i("HAG", "Start...");

        Executor mainExecutor = this.getMainExecutor();
        Executor workExecutor = Executors.newFixedThreadPool(3);

        // Run a task specified by a Runnable Object asynchronously.
        CompletableFuture<Integer> cf1 = CompletableFuture.supplyAsync(() -> longProcess(R.id.textView1, 100), workExecutor);
        CompletableFuture<Integer> cf2 = CompletableFuture.supplyAsync(() -> longProcess(R.id.textView2, 50), workExecutor);

        // Wait until all are finishec
        CompletableFuture<Void> cfall = CompletableFuture.anyOf(cf1, cf2)
                .thenRunAsync(() -> {
                    doStop = true;
                }, mainExecutor);
        Log.i("HAG", " ... end");
    }
}