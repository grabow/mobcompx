package edu.offenburg.example_8_2;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public class WorkerThread extends Thread {

    Handler mHandler;
    int counter = 0;

    @Override
    public void run() {
        Looper.prepare();
        mHandler = new Handler() {
            public void handleMessage(Message msg) {
                // Act on the message
                Log.i("HAG", "Counter: " + counter);
            }
        };
        Looper.loop();
    }
}