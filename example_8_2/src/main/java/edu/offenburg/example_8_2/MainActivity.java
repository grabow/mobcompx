package edu.offenburg.example_8_2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    WorkerThread myWorker1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myWorker1 = new WorkerThread();
        myWorker1.start();
    }

    public void callThreads(View v) {
        Message m = new Message();
        m.arg1 = 111;
        myWorker1.mHandler.post( () -> {
            for(int i = 0; i<10; i++) {
                Log.i("HAG" , "C: " + i );
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
