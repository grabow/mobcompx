package com.example.lifecycle_aware2;

import android.util.Log;
import android.widget.TextView;

public class Thread2 {
    int count = 100000;
    static boolean doRun = true;
    public void countDown(TextView tv) {
        new Thread(
                ()->{
                    while(true) {
                        if (doRun) {
                            count--;
                            Log.i("HAG", "Counter2: " + count);
                            tv.post( ()->{ tv.setText(count + "");});
                        } else {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
                }
        ).start();
    }
}
