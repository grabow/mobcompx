package com.example.lifecycle_aware2;

import android.util.Log;
import android.widget.TextView;

public class Thread3 {
    int count = 0;
    static boolean doRun = true;
    public void getRandom(TextView tv) {
        new Thread(
                ()->{
                    while(true) {
                        if (doRun) {
                            count = (int)(Math.random() * 100);
                            Log.i("HAG", "Counter: " + count);
                            tv.post( ()->{ tv.setText(count + "");});
                        } else {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
                }
        ).start();
    }
}
