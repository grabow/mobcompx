package com.example.lifecycle_aware2;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;

public class Thread1 implements DefaultLifecycleObserver {
    int count = 0;
    public boolean doRun = true;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(LifecycleOwner owner) { Log.i("HAG", "t1: The onCreate() event"); }

    /**
     * Called when the activity is about to become visible.
     */
    @Override
    public void onStart(LifecycleOwner owner) {
        Log.i("HAG", "t1: The onStart() event");
    }

    /**
     * Called when the activity has become visible.
     */
    @Override
    public void onResume(LifecycleOwner owner) {
        Log.i("HAG", "t1: The onResume() event");
    }

    /**
     * Called when another activity is taking focus.
     */
    public void onPause(LifecycleOwner owner) {
        Log.i("HAG", "t1: The onPause() event");
    }

    /**
     * Called when the activity is no longer visible.
     */
    public void onStop(LifecycleOwner owner) {
        Log.i("HAG", "t1: The onStop() event");
    }

    /**
     * Called just before the activity is destroyed.
     */
    public void onDestroy(LifecycleOwner owner) {
        Log.i("HAG", "t1: The onDestroy() event");
    }

    public void countUp(TextView tv) {
        new Thread(
                () -> {
                    while (true) {
                        if (doRun) {
                            count++;
                            Log.i("HAG", "Counter1: " + count);
                            tv.post(() -> {
                                tv.setText(count + "");
                            });
                        } else {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
                }
        ).start();
    }
}
