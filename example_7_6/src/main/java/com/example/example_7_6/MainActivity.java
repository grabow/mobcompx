package com.example.example_7_6;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;



    public class MainActivity extends AppCompatActivity {



        @Override
        protected void onCreate(Bundle savedInstanceState) {
            Log.i("HAG", "onCreate()");
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
        }

        @Override
        protected void onPause() {
            super.onPause();
        }

        Runnable runnable = () -> {
            for (int i = 0; i < 100; i++) {
                Log.i("HAG", "Thread: " + Thread.currentThread().getName() + ", " + i);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        public void onPressed(View v)  {
            Integer result = null;
            Toast.makeText(this, "Pressed!", Toast.LENGTH_SHORT).show();
            Log.i("HAG", "Start...");

            // Run a task specified by a Runnable Object asynchronously.
            CompletableFuture<Void> cfuture = CompletableFuture.runAsync(runnable);

            Log.i("HAG", " ... end");
        }
}