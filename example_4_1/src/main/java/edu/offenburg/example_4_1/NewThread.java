package edu.offenburg.example_4_1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class NewThread extends AppCompatActivity {
    Button btn = null;
    Activity act = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_thread);
        btn = findViewById(R.id.myButton);
    }

    public void onPressed(View v) {

        Toast.makeText(this, "pressed", Toast.LENGTH_SHORT).show();

        Log.i("HAG", "GEDRÜCKT!!!!!!");

        new Thread(
                () -> {
                        for (int i = 0; i < 100; i++) {
                            Log.i("HAG", "i:" + i);
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            // Access the GUI :-;
                            Toast.makeText(act,"Running:" + i, Toast.LENGTH_SHORT).show();
                        }
                }
        ).start();
    }

    /*
    Fuer lambas setze in build.gradle die language auf Java_8
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }

    run() inmplementiert ein functional interface, kann daher durch lambas ersetz werden
    new Thread(()->{ ... }).start();
     */
}
