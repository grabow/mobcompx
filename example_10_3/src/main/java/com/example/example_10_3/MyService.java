package com.example.example_10_3;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {
    public MyService() {
    }



    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //TODO do something useful
        Log.i("HAG", "onStartCommand called" );
        new Thread(() -> {
            int i = 0;
            while(true) {
               Log.i("HAG", "Service running: " + i++);
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        return Service.START_NOT_STICKY;
    }
}