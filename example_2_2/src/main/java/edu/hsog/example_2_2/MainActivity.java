package edu.hsog.example_2_2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    double c = 0;
    EditText etx = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        c = Math.random();
        Log.i("HAG", "Zahl: " + c);

        // android:saveEnabled="false"
        etx = findViewById(R.id.myName);
        etx.setText("Ihr Name ..." );
        Log.i("HAG", "onCreate executed...");
    }


}