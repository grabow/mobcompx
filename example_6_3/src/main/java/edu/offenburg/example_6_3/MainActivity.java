package edu.offenburg.example_6_3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextClock;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextClock tc = findViewById(R.id.myTextClock);
        tc.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        Log.i("HAG", "onclick");
        TextClock tc = findViewById(R.id.myTextClock);
        tc.setBackgroundResource(R.mipmap.ic_launcher);
    }

}
