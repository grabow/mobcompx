package com.example.example_7_11;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import java9.util.concurrent.CompletableFuture;
import java9.util.function.Supplier;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("HAG", "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public int longProcess(int max) {
        for (int i = 0; i < max; i++) {
            Log.i("HAG", "Thread: " + Thread.currentThread().getName() + ", " + i);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return max;
    }

    Supplier<Integer> supplier = () -> {
        return longProcess(50);
    };

    Runnable sayHallo = () -> {
        String res = "Fertig !!! ";
        Log.i("HAG", Thread.currentThread().getName() + ": " + res);
        Toast.makeText(this, res, Toast.LENGTH_SHORT).show();
    };


    CompletableFuture<Void> doNextCompletable() {
        return CompletableFuture.allOf(
                (CompletableFuture.runAsync(()->longProcess(110))),
                (CompletableFuture.runAsync(()->longProcess(110))));
    }


    public void onPressed(View v) throws ExecutionException, InterruptedException {
        Toast.makeText(this, "Pressed!", Toast.LENGTH_SHORT).show();
        Log.i("HAG", "Start...");

        Executor mainExecutor = this.getMainExecutor();
        Executor workExecutor = Executors.newFixedThreadPool(3);

        // Run a task specified by a Runnable Object asynchronously.
        CompletableFuture<Integer> cf1 = CompletableFuture.supplyAsync(()->longProcess(50), workExecutor);
        CompletableFuture<Integer> cf2 = CompletableFuture.supplyAsync(()->longProcess(60), workExecutor);
        CompletableFuture<Integer> cf3 = CompletableFuture.supplyAsync(()->longProcess(30), workExecutor);


        // Wait until all are finishec
        CompletableFuture<Void> cfall = CompletableFuture.allOf(cf1, cf2, cf3)
                .thenRunAsync( sayHallo, mainExecutor )
                .thenComposeAsync(
                        (dummy)-> CompletableFuture.allOf(
                                CompletableFuture.runAsync(()->longProcess(150), workExecutor),
                                CompletableFuture.runAsync(()->longProcess(150), workExecutor))
                )
                .thenRunAsync( ()->longProcess(30), workExecutor)
                .thenRunAsync( sayHallo, mainExecutor )
                .orTimeout(5, TimeUnit.SECONDS)
                .whenComplete((dummy, error) -> {
                    if (error == null) {
                        Log.i("HAG", "Complete!");
                    } else {
                        Log.i("HAG","Error: Timeout!");
                    }
                })
                ;
        Log.i("HAG", " ... end");
    }
}