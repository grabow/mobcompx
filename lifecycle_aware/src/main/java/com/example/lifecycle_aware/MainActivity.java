package com.example.lifecycle_aware;

import android.os.Bundle;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {

    Thread1 t1 = null;
    Thread2 t2 = null;
    Thread3 t3 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        t1 = new Thread1();
        t2 = new Thread2();
        t3 = new Thread3();

        t1.countUp(findViewById(R.id.textView1));
        t2.countDown(findViewById(R.id.textView2));
        t3.getRandom(findViewById(R.id.button));
    }

    @Override
    protected void onResume() {
        super.onResume();
        t1.doRun = true;
        t2.doRun = true;
        t3.doRun = true;

    }

    @Override
    protected void onPause() {
        super.onPause();
        t1.doRun = false;
        t2.doRun = false;
        t3.doRun = false;
    }
}