package com.example.lifecycle_aware;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;

public class Thread1 {
    int count = 0;
    public boolean doRun = true;
    public void countUp(TextView tv) {
        new Thread(
                ()->{
                    while(true) {
                        if (doRun) {
                            count++;
                            Log.i("HAG", "Counter1: " + count);
                            tv.post( ()->{ tv.setText(count + "");});
                        } else {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
                }
        ).start();
    }
}
