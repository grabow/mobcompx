package edu.hsog.example_13_2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void doHardStuff( Function<String,Boolean> myFkt ) {
        try {
            // Do some hard computation
            String str = Double.toString(Math.random());
            // Open ports
            // ...

            boolean ok = myFkt.apply( str );

            // Close ports
        } catch (Exception e) {
            Log.e("HAG", "Error: " + e.getMessage());
        }
    }

    private boolean printNice(String str) {
        Log.i("HAG", "*******************");
        Log.i("HAG", "* At " + new Date().getTime() + ": " + str);
        Log.i("HAG", "*******************");
        return true;
    }

    private boolean printCompact(String str) {
        Log.i("HAG", str);
        return true;
    }

    Function<String, Boolean> printNice2 = new Function<String, Boolean>() {
        @Override
        public Boolean apply(String str) {
            Log.i("HAG", "*******************");
            Log.i("HAG", "* At " + new Date().getTime() + ": " + str);
            Log.i("HAG", "*******************");
            return true;
        }
    };

    Function<String, Boolean> printNice3 = (str) -> {
        Log.i("HAG", "*******************");
        Log.i("HAG", "* At " + new Date().getTime() + ": " + str);
        Log.i("HAG", "*******************");
        return true;
    };

    Function<String, Boolean> printNice4 = (str) -> {
        boolean ok = printNice(str);
        return ok;
    };

    Function<String, Boolean> printNice5 = (str) -> printNice(str);

    Function<String, Boolean> printNice6 = this::printNice;

    Consumer<String> printNice7 = (str) -> printNice(str);

    Supplier<Boolean> printNice8 = () -> printNice("dummy");

    Runnable printNice9 = () -> printNice("dummy");


    public void onClick(View v) {
        doHardStuff( (str) -> printNice(str) );
    }

}