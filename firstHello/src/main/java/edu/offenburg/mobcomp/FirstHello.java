package edu.offenburg.mobcomp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class FirstHello extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_hello);
    }
}