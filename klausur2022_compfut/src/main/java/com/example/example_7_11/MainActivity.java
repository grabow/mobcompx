package com.example.example_7_11;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import java9.util.concurrent.CompletableFuture;
import java9.util.function.Supplier;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("HAG", "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public int longProcess(int stime) {
        // Thread sleeps for
        for (int i = 0; i < stime; i++) {
            Log.i("HAG", "Thread: " + Thread.currentThread().getName() + ", " + i);
            try {
                Thread.sleep(stime * 10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return stime;
    }

    Runnable sayHallo = () -> {
        Toast.makeText(this, "Fertig!", Toast.LENGTH_SHORT).show();
    };

    public void onPressed(View v) throws ExecutionException, InterruptedException {
        Toast.makeText(this, "Pressed!", Toast.LENGTH_SHORT).show();
        Log.i("HAG", "Start...");

        Executor mainExecutor = this.getMainExecutor();
        Executor workExecutor = Executors.newFixedThreadPool(3);

        // Run a task specified by a Runnable Object asynchronously.
        CompletableFuture<Integer> cf1 = CompletableFuture.supplyAsync(()->longProcess(20), workExecutor);
        CompletableFuture<Integer> cf2 = CompletableFuture.supplyAsync(()->longProcess(30));

        // Wait until all are finishec
        CompletableFuture<Void> cfall = CompletableFuture.allOf(cf1, cf2)
                .thenRunAsync( sayHallo, mainExecutor );
        Log.i("HAG", " ... end");
    }
}