package edu.offenburg.example_7_2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onPressed(View v) {

        Toast.makeText(this, "Pressed!", Toast.LENGTH_SHORT).show();
        Log.i("HAG", "Start...");

        Runnable runnable = () -> {
            String name = Thread.currentThread().getName();
            for (int i = 0; i < 100; i++) {
                Log.i("HAG", "Thread: " + name + ", " + i);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        ExecutorService executor = Executors.newSingleThreadExecutor();
        
        executor.submit(runnable);
        // executor.submit(runnable);

    }
}
