package com.example.example_10_1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onPressed(View v) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);  //ACTION_VIEW = "android.intent.action.VIEW"
        // intent.addCategory(Intent.CATEGORY_APP_MUSIC);
        // intent.setType("text/html");
        intent.setData(Uri.parse("http://www.hs-offenburg.de"));
        startActivity(intent);
    }
}