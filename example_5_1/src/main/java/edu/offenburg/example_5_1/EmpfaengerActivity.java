package edu.offenburg.example_5_1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class EmpfaengerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empfaenger);

        final Bundle extras = getIntent().getExtras();
        String str = extras.getString("MyTag");

        TextView tv = findViewById(R.id.myTextView);
        tv.setText( str );

    }
}