package edu.offenburg.example_5_1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onPressed(View v) {

        Toast.makeText(this, "pressed", Toast.LENGTH_SHORT).show();
        Log.i("HAG", "GEDRÜCKT!");

        Intent intent = new Intent(this, EmpfaengerActivity.class);
        intent.putExtra("MyTag", "This is my Text!");
        startActivity(intent);

    }
}
