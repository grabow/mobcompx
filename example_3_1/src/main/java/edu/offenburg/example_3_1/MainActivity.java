package edu.offenburg.example_3_1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onMyButtonPressed(View v) {

        Button myButton = (Button) findViewById(R.id.secondBtn);
        myButton.setText("YO!!!");

        Log.i("HAG", "Button wurde gedrückt!");

        Toast.makeText(this, "Alles gut!",Toast.LENGTH_SHORT).show();

    }
}
