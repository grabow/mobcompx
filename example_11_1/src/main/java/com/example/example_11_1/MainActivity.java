package com.example.example_11_1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onZugPressed(View v) {
        // Opens Street View between two Pyramids in Giza. The values passed to the
        // cbp parameter will angle the camera slightly up, and towards the east.
        Uri gmmIntentUri = Uri.parse("google.streetview:cbll=48.4590306,7.9434895&cbp=0,30,0,0,-15");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        // mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    public void onLondonPressed(View v) {
        // Uses a PanoID to show an image from Maroubra beach in Sydney, Australia
        Uri gmmIntentUri = Uri.parse("google.streetview:cbll=51.5042562,-0.1192135");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    double randomWithRange(double min, double max){
        double range = (max - min) + 1;
        return (double)(Math.random() * range) + min;
    }

    public void onRandom(View v) {
        double lng = randomWithRange(-180.0, +180.0);
        double lat = randomWithRange(-90.0, +90.0);
        // Creates an Intent that will load a map of San Francisco
        Uri gmmIntentUri = Uri.parse("geo:"+lat+","+lng+"?z=4");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

}