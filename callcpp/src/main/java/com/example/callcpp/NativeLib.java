package com.example.callcpp;

public class NativeLib {
    static {
        System.loadLibrary("doAddLib");
    }

    // Neue native Methode für einen Eingabestring
    public native String processStringNative(String input);
}