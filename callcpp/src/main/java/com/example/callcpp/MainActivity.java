package com.example.callcpp;

import android.os.Bundle;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NativeLib nativeLib = new NativeLib();

        // String an die C++-Funktion übergeben
        String input = "Hallo Android";
        String result = nativeLib.processStringNative(input);

        // Ergebnis anzeigen
        TextView textView = findViewById(R.id.textView);
        textView.setText(result);

    }
}