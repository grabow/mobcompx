#include "jni.h"
#include "doAdd.h"
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_callcpp_NativeLib_processStringNative(JNIEnv *env, jobject thiz, jstring input) {
    // Konvertiere jstring zu std::string
    const char *input_cstr = env->GetStringUTFChars(input, nullptr);

    // Rufe die C++-Funktion auf
    std::string result = processString(input_cstr);

    // Freigeben der Ressourcen
    env->ReleaseStringUTFChars(input, input_cstr);

    // Ergebnis als jstring zurückgeben
    return env->NewStringUTF(result.c_str());
}