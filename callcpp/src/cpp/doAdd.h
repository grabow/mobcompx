#ifndef DOADD_H
#define DOADD_H

#include <string>

std::string processString(const std::string &input);

#endif // DOADD_H