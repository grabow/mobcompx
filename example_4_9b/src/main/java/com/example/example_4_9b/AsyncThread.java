package com.example.example_4_9b;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

public class AsyncThread extends AsyncTask<Integer, String, Integer> {

    MyProgress myProgress;
    AsyncThread(MyProgress myProgress) {
        this.myProgress=myProgress;
    }

    int myGlobalCounter = 0;
    Activity act;
    TextView textView;

    @Override
    protected Integer doInBackground(Integer... counter) {
        myGlobalCounter = (int) counter[0];

        while (myGlobalCounter < 100) {

            // if (!doRun) break;

            Log.i("HAG", "Loop No. " + myGlobalCounter + ": " + myGlobalCounter);

            publishProgress("" + myGlobalCounter);

            myGlobalCounter++;

            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return (Integer) (myGlobalCounter);
    }


    protected void onProgressUpdate(String... values) {
        myProgress.upFkt(values[0]);
    }


    @Override
    protected void onPostExecute(Integer resultCounter) {
        myProgress.upFkt("Ende: " + resultCounter);
    }
}
