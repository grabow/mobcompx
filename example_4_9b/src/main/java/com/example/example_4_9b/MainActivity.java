package com.example.example_4_9b;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener, MyProgress {

    TextView myTextView;
    int startCounter = 1;
    AsyncThread myAsyncThread;
    boolean doRun = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myTextView = (TextView) findViewById(R.id.myTextView);
        myTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        myAsyncThread = new AsyncThread(this);
        myTextView.setText("OK");
        myAsyncThread.execute(new Integer[] { (Integer) startCounter });
    }

    @Override
    public void onPause() {
        super.onPause();
        doRun = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        doRun = true;
    }

    @Override
    public void upFkt(String str) {
        TextView textView = (TextView) findViewById(R.id.myTextView);
        textView.setText( str );
    }
}