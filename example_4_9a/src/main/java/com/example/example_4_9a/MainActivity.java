package com.example.example_4_9a;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {

    TextView myTextView;
    int myGlobalCounter = 0;
    int startCounter = 1;
    AsyncThread myAsyncThread;
    boolean doRun = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myTextView = (TextView) findViewById(R.id.myTextView);
        myTextView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        myAsyncThread = new AsyncThread();
        myTextView.setText("OK");
        myAsyncThread.execute(new Integer[] { (Integer) startCounter });
    }

    public class AsyncThread extends AsyncTask<Integer, String, Integer> {
        @Override
        protected Integer doInBackground(Integer... counter) {
            myGlobalCounter = (int) counter[0];

            while (myGlobalCounter < 100) {

                // if (!doRun) break;

                Log.i("HAG", "Loop No. " + myGlobalCounter + ": " + myGlobalCounter);

                publishProgress("" + myGlobalCounter);

                myGlobalCounter++;

                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return (Integer) (myGlobalCounter);
        }


        protected void onProgressUpdate(String... values) {
            // TODO Auto-generated method stub
            TextView textView = (TextView) findViewById(R.id.myTextView);
            textView.setText( myGlobalCounter +"");
        }


        @Override
        protected void onPostExecute(Integer resultCounter) {
            myTextView.setText("Ende: " + resultCounter);
            startCounter = resultCounter;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        doRun = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        doRun = true;
    }
}