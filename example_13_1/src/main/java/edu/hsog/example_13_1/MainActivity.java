package edu.hsog.example_13_1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Function;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void doHardStuff(Function<String, Boolean> myFkt ) {
        try {
            // Do some hard computation
            String str = Double.toString(Math.random());
            // Open ports
            // ...

            myFkt.apply( str );
            // boolean ok = printNice( str );


            // Close ports
        } catch (Exception e) {
            Log.e("HAG", "Error: " + e.getMessage());
        }
    }

    private boolean printNice(String str) {
        Log.i("HAG", "*******************");
        Log.i("HAG", "* At " + new Date().getTime() + ": " + str);
        Log.i("HAG", "*******************");
        return true;
    }


    Function<String, Boolean> printNice2 = str -> printCompact(str);



    private boolean printCompact(String str) {
        Log.i("HAG", str);
        return true;
    }

    public void onClick(View v) {

        doHardStuff( str -> printNice(str)  );

    }

    Consumer<String> printNice3 = new Consumer<String>() {
        @Override
        public void accept(String str) {
            Log.i("HAG", str);
        }
    };

    Consumer<String> printNice4 = (str)-> Log.i("HAG", str);


}