package com.example.example_4_9c;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.Consumer;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {

    TextView myTextView;
    int startCounter = 1;
    AsyncThread myAsyncThread;
    boolean doRun = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myTextView = (TextView) findViewById(R.id.myTextView);
        myTextView.setOnClickListener(this);
    }

    Consumer<String> upFkt = (str) -> {
        myTextView = (TextView) findViewById(R.id.myTextView);
        myTextView.setText( str +"");
    };

    Consumer<Integer> endFkt = (i) -> {
        myTextView = (TextView) findViewById(R.id.myTextView);
        myTextView.setText( "Ende: " + i);
    };

    @Override
    public void onClick(View v) {
        myAsyncThread = new AsyncThread(upFkt, endFkt);
        myTextView.setText("OK");
        myAsyncThread.execute(new Integer[] { (Integer) startCounter });
    }

    @Override
    public void onPause() {
        super.onPause();
        doRun = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        doRun = true;
    }
}