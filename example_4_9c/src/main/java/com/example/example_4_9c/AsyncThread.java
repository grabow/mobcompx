package com.example.example_4_9c;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import androidx.core.util.Consumer;

public class AsyncThread extends AsyncTask<Integer, String, Integer> {

    Consumer<String> upFkt;
    Consumer<Integer> endFkt;
    AsyncThread(Consumer<String> upFkt, Consumer endFkt) {
        this.endFkt=endFkt;
        this.upFkt=upFkt;
    }

    int myGlobalCounter = 0;
    Activity act;

    @Override
    protected Integer doInBackground(Integer... counter) {
        myGlobalCounter = (int) counter[0];

        while (myGlobalCounter < 100) {

            // if (!doRun) break;

            Log.i("HAG", "Loop No. " + myGlobalCounter + ": " + myGlobalCounter);

            publishProgress("" + myGlobalCounter);

            myGlobalCounter++;

            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return (Integer) (myGlobalCounter);
    }


    protected void onProgressUpdate(String... values) {
        // TODO Auto-generated method stub
        upFkt.accept(values[0]);
    }

    @Override
    protected void onPostExecute(Integer resultCounter) {
        endFkt.accept(resultCounter);
    }
}
