package edu.hsog.qrcodereader;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class Reader extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);
    }

    public void onClick(View v) {
        Intent qrIntent = new Intent("la.droid.qr.scan");
        startActivityForResult(qrIntent, 123);
    }

    public void onActivityResult(int reC, int resC, Intent data) {
        super.onActivityResult(reC, resC, data);
        String result = data.getExtras().getString("la.droid.qr.result");
        Toast.makeText(this, "QRCODE: " + result, Toast.LENGTH_LONG).show();
    }
}