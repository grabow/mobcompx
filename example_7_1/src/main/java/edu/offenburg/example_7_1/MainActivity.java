package edu.offenburg.example_7_1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private void doMyJob() {
        String name = Thread.currentThread().getName();
        for (int i = 0; i < 100; i++) {
            Log.i("HAG", "Thread: " + name + ", " + i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void onPressed(View v) {

        Toast.makeText(this, "Pressed!", Toast.LENGTH_SHORT).show();
        Log.i("HAG", "Start...");
        Runnable runnable = () -> {
                    String name = Thread.currentThread().getName();
                    for (int i = 0; i < 100; i++) {
                        Log.i("HAG", "Thread: " + name + ", " + i);
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                };

        new Thread( ()->doMyJob() ).start();
    }
}
