package edu.hsog.tflite_04;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.Tensor;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Interpreter interpreter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void pressed(View v) {
        MappedByteBuffer model = TFLiteHelperHG.loadTFModelFromAssets(this, "model_float16.tflite");
        Interpreter.Options options = TFLiteHelperHG.getInterpreterOptions(false, 0);
        interpreter = new Interpreter(model, options);

        Bitmap imgBitmap = TFLiteHelperHG.loadBitmapFromAsset(this, "elephant.jpg");
        TensorBuffer inputBuffer = TFLiteHelperHG.getTensorBufferFromBitmap(imgBitmap);

        TensorBuffer resultBuffer = TensorBuffer.createFixedSize( new int[] {1, 1000}, DataType.FLOAT32);
        float[][] result = new float[1][1000];

        long before = System.currentTimeMillis();
        for(int i=0; i<100; i++)
            interpreter.run(inputBuffer.getBuffer(), resultBuffer.getBuffer().rewind());
        long duration = System.currentTimeMillis() - before;
        Log.i("HAG", "Used time:" + duration);

        int maxAt = TFLiteHelperHG.getIndexOfMax(resultBuffer.getFloatArray());
        Log.i("HAG", "Max at:" + maxAt);
        // LookUp for label at: https://storage.googleapis.com/download.tensorflow.org/data/imagenet_class_index.json

        String[] labels = TFLiteHelperHG.loadLables(this, "imagenet-labels.txt");
        List<Recognition> recognitions = TFLiteHelperHG.getSortedResult(resultBuffer.getFloatArray(), labels, 5, 0);
        Log.i("HAG", recognitions.toString());
    }


}