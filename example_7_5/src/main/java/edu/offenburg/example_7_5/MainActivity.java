package edu.offenburg.example_7_5;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

public class MainActivity extends AppCompatActivity {

    CompletableFuture<Integer> cf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onPause() {
        super.onPause();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    static <T, R> Function<T, R> supplierToFunc(Supplier<R> supplier) {
        return t -> {
            // t is ignored!!!
            R erg = supplier.get();
            return erg;
        };
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    static Supplier<Integer> supplierFromURL(int maxCount) {
        return () -> {
            for (int i = 0; i < maxCount; i++) {
                Log.i("HAG", "Thread: " + Thread.currentThread().getName() + ", " + i);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return 123;
        };
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void onPressed(View v)  {
        Integer result = null;
        Toast.makeText(this, "Pressed!", Toast.LENGTH_SHORT).show();
        Log.i("HAG", "Start...");

        Supplier<Integer> longTask = () -> {
            for (int i = 0; i < 100; i++) {
                Log.i("HAG", "Thread: " + Thread.currentThread().getName() + ", " + i);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return 123;
        };



        // executor = Executors.newSingleThreadExecutor();
        // thenApply, thenAccept, thenRun

        Executor exeMainThread = ContextCompat.getMainExecutor(this);

        cf = CompletableFuture.supplyAsync( supplierFromURL(111) )
                                 .thenApplyAsync( (res) -> {
                                     Log.i("HAG", "Thread: " + Thread.currentThread().getName() + ", ResultA: " + res);
                                     ((Button)findViewById(R.id.button)).setText("FERTIG!");
                                     return "Geschafft in " + res + " Iterationen!";
                                 }, exeMainThread)
                                 .thenAcceptAsync( (erg) -> {
                                     Log.i("HAG", "Thread: " + Thread.currentThread().getName() + ", ResultB: " + erg);
                                 }).thenApplyAsync( supplierToFunc(longTask) ) ;

        Log.i("HAG", "Future done? " + cf.isDone());

    }


}
